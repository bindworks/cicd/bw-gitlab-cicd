#!/usr/bin/env bash

# Usage: setup-jfrog-dart.sh <environment-variable-name-with-jfrog-token> <jfrog-repository> 
#
# <jfrog-repository> defaults to https://bindworks.jfrog.io/artifactory/api/pub/internal-pub
#

REPOSITORY_URL="${2:-https://bindworks.jfrog.io/artifactory/api/pub/internal-pub}"

if [ "$1" = "" ]; then
    echo "❗ Usage: $0 <environment-variable-name-with-jfrog-token>" >&2
    exit 1
fi

export ACCESS_TOKEN="${!1}"

if [ "$ACCESS_TOKEN" = "" ]; then
    echo "❗ Variable $1 does not contain any JFROG access token, skipping the auth" >&2
    exit 0
fi

echo "🌟 Setting up JFROG repository $REPOSITORY_URL..." >&2
dart pub token add --env-var "$1" "$REPOSITORY_URL"
