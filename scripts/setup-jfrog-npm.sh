#!/usr/bin/env bash

# Usage: setup-jfrog-npm.sh <jfrog-username> <environment-variable-name-with-jfrog-token> <jfrog-repository> 
#
# <jfrog-repository> defaults to https://bindworks.jfrog.io/artifactory/api/npm/internal-npm/
#                    it MUST end with a slash!!


REPOSITORY_URL="${3:-https://bindworks.jfrog.io/artifactory/api/npm/internal-npm/}"

if [ "$1" = "" ]; then
    echo "❗ Usage: $0 <environment-variable-name-with-jfrog-token>" >&2
    exit 1
fi

USERNAME="${1}"
ACCESS_TOKEN="${!2}"

if [ "$ACCESS_TOKEN" = "" ]; then
    echo "❗ Variable $2 does not contain any JFROG access token" >&2
    exit 1
fi

echo "🌟 Setting up JFROG repository $REPOSITORY_URL..." >&2
PASSWORD=$(echo -n "$ACCESS_TOKEN" | base64 -w 0)
REPOSITORY_NO_PROTOCOL=$(echo "$REPOSITORY_URL" | sed 's#^https\?:##')
cat > .npmrc <<EOF
@bindworks:registry=$REPOSITORY_URL
$REPOSITORY_NO_PROTOCOL:_password=$PASSWORD
$REPOSITORY_NO_PROTOCOL:username=$USERNAME
$REPOSITORY_NO_PROTOCOL:email=cicd@bindworks.eu
$REPOSITORY_NO_PROTOCOL:always-auth=true
EOF
