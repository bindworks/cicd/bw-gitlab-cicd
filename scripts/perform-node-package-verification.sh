#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";
source "$SCRIPT_DIR/common.inc.sh"

cicd_check_title "npm ci"
npm ci

cicd_check_title "npm run build"
npm run build && cicd_accumulate_success "Node build passed" || cicd_accumulate_error "Node build failed"

cicd_check_title "npm run test"
npm run test && cicd_accumulate_success "Node tests passed" || cicd_accumulate_error "Node tests failed"

cicd_check_title "changelog messages"
cicd_check_changelog_not_empty
cicd_check_changelog_bumpy

cicd_check_title "npm publish"
npm publish --dry-run . && cicd_accumulate_success "npm publish --dry-run passed" || cicd_accumulate_error "npm publish --dry-run failed"

cicd_checks_finished
