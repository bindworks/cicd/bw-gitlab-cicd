#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";
source "$SCRIPT_DIR/common.inc.sh"

cd application

if [ "$BW_EXTERNAL_PATH" != "" ]; then
    cd "$BW_EXTERNAL_PATH"
fi

cicd_step_title "Patch pubspec.yml"
GIT_HASH=$(git rev-parse HEAD | sed 's#^\(.......\).*#\1#')
cp pubspec.yaml pubspec.yaml.bak
cat pubspec.yaml.bak | \
    sed -e '/^version:/ s#version:\(\s*\)\(\S\+\)\(\s*\)#version:\1\2+bw.'"$GIT_HASH"'\3#' | \
    grep -v '^publish_to:' | \
    ( cat && echo "" && echo "publish_to: https://dart.cloudsmith.io/bindworks/dart/" ) > pubspec.yaml

git diff --color

cicd_step_title "Publish to Cloudsmith"
git rm -rq example || true
rm -rf example
dart pub publish -f

